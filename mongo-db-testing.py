import pymongo
import time

client = pymongo.MongoClient("mongodb+srv://jodi:E5FhYeQRyKqdbRv@cluster0.u3nib.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", maxPoolSize=1)

db = client['sample_airbnb']
col = db["listingsAndReviews"]

def find_one():
    data = col.find_one()
    print(data)

def insert_one():
    data = {"name": "Jodi Listing", "summary": "this is sample 2"}
    x = col.insert_one(data)

def query_mongo():
    query = {"name": {"$regex": "^S"}}
    # query = {"name": "Jodi Listing"}

    doc = col.find(query)

    for i in doc:
        print(i)

def find_many():
    for x in col.find():
        print(x)

if __name__ == "__main__":
    start = time.time()

    find_one()
    # find_many()
    # insert_one()
    query_mongo()

    elapsed = (time.time() - start)
    print(elapsed)
